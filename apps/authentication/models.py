# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""
from django.contrib.auth.models import User
from django.db import models

# Create your models here.
# Create your models here.
class UserExtended(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
	empresa = models.CharField(max_length=64, blank=True)
	cargo = models.CharField(max_length=64, blank=True)

	def __str__(self):
		return str(self.user) + " - " + str(self.empresa)
