# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""
from django.contrib.auth.models import User
from django.contrib import admin

# Register your models here.
from .models import UserExtended


def get_related_field(name, admin_order_field=None, short_description=None):
    related_names = name.split('__')

    def dynamic_attribute(obj):
        for related_name in related_names:
            obj = getattr(obj, related_name)
        return obj

    dynamic_attribute.admin_order_field = admin_order_field or name
    dynamic_attribute.short_description = short_description or related_names[-1].title().replace('_', ' ')
    return dynamic_attribute


class RelatedFieldAdmin(admin.ModelAdmin):
    def __getattr__(self, attr):
        if '__' in attr:
            return get_related_field(attr)

        # not dynamic lookup, default behaviour
        return self.__getattribute__(attr)


class UserAdmin(RelatedFieldAdmin):
    list_display = ('empresa', 'cargo' ,'user__email', 'user__date_joined', 'user__first_name', 'user__last_name')

      

admin.site.register(UserExtended, UserAdmin)

