# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

# Create your views here.
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from .forms import LoginForm, SignUpForm
from .models import UserExtended
from django.contrib.auth.models import User


def login_view(request):
    form = LoginForm(request.POST or None)

    msg = None

    if request.method == "POST":

        if form.is_valid():
            username = form.cleaned_data.get("username")
            password = form.cleaned_data.get("password")
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("/")
            else:
                msg = 'Credenciales Invalidad'
        else:
            msg = 'Error en la validación del formulario'

    return render(request, "accounts/login.html", {"form": form, "msg": msg})


def register_user(request):
    msg = None
    success = False

    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get("username")
            raw_password = form.cleaned_data.get("password1")
            user = authenticate(username=username, password=raw_password)

            msg = 'Usuario Creado Correctamente - Ingresar a <a href="/login">Iniciar Sesión</a>.'
            success = True

            # return redirect("/login/")
            empresa = request.POST.get('empresa')
            cargo = request.POST.get("cargo")

            print(empresa,cargo)

            user_new = UserExtended()
            user_id = User.objects.get(email=form.cleaned_data.get("email"))
            user_new.user = user_id
            user_new.empresa = empresa
            user_new.cargo = cargo
            user_new.save()

        else:
            msg = 'Formulario no es valido'
    else:
        form = SignUpForm()

    return render(request, "accounts/register.html", {"form": form, "msg": msg, "success": success})
