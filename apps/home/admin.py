# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

from django.contrib import admin
from .models import City, Client, Users
from import_export.admin import ImportExportModelAdmin
#admin.site.register(Simulacion)
admin.site.register(City)
admin.site.register(Client)
admin.site.register(Users)

