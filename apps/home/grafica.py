import plotly.graph_objects as go
from plotly.subplots import make_subplots
import pandas as pd
from .models import Simulacion, Ahp, Consolidado, Distancias, Resultados, Graficas



def visualizacion(myuuid,dff,dff2,escenario,fo,p8,p28):
    print(myuuid)
    simulacion = Simulacion.objects.filter(data_id=str(myuuid)).exists()
    print(simulacion)
    if simulacion:
        simulacion = Simulacion.objects.get(data_id=str(myuuid))
        simulacion.funcion_objetivo = fo
        simulacion.per_8 = p8
        simulacion.per_28 = p28
        simulacion.save()
        mes = []
        valor = []
        tipo = []
        for i in range(len(dff)):
            mes.append(dff.iloc[i]['mes'])
            valor.append(dff.iloc[i][escenario])
            tipo.append(dff.iloc[i]['Tipo'])
            #print(dff.iloc[i]['mes'])
            #print(dff.iloc[i]['med'])
            #print(dff.iloc[i]['pes'])
            #print(dff.iloc[i]['Tipo'])
        grafica = Graficas()
        grafica.simulacion_id = simulacion
        grafica.mes = mes
        grafica.valor = valor
        grafica.tipo = tipo
        grafica.save()
        veh = []
        purcha = []
        sell = []
        tipo2 = []
        for i in range(len(dff2)):
            veh.append(dff2.iloc[i]['veh'])
            purcha.append(dff2.iloc[i]['purcha'])
            sell.append(dff2.iloc[i]['sell'])
            tipo2.append(dff2.iloc[i]['Tipo'])
        resultado = Resultados()
        resultado.simulacion_id = simulacion
        resultado.veh = veh
        resultado.purcha = purcha
        resultado.sell = sell
        resultado.tipo = tipo2
        resultado.save()
    """ fig = make_subplots(
        rows=1, cols=2,
        shared_xaxes=True,
        vertical_spacing=0.01,
        specs=[[{"type": "table"},{"type": "pie"}]]
    )

    fig.add_trace(
        go.Pie(labels=["Combustible", "Administración", "Financiación", "Mantenimiento", "Descuentos"],
                                values=[95, 85, 75, 95, 101], textinfo='percent',
                                insidetextorientation='radial', hole=.3),
        row=1, col=2
    )


    fig.add_trace(
        go.Table(
            header=dict(),
            cells=dict(values=[["Combustible", "Administración", "Financiación", "Mantenimiento", "Descuentos", "Total"], [95, 85, 75, 95, 0, 0]])
        ),
        row=1, col=1
    )
    fig.update_layout(
        showlegend=True,
        paper_bgcolor='#f8f9fa',
        title={
            'text': "Costos Totales",
            'y':0.9,
            'x':0.5,
            'xanchor': 'center',
            'yanchor': 'top'},
            legend=dict(
                orientation="h",
                yanchor="bottom",
                #y=1.02,
                xanchor="right",
                x=1
            )
    )
    fig.write_image("fig1.png")

    fig2 = make_subplots(
        rows=1, cols=2,
        shared_xaxes=True,
        vertical_spacing=0.01,
        specs=[[{"type": "table"},{"type": "bar"}]]
    )
    branches = ['2020<br>2030', '2030<br>2040', '2040<br>2050']
    fy = [23,17,35]
    sy = [20, 23, 30]
    fig2.add_trace(
        go.Bar(
        x = branches,
        y = fy,
        name = 'Poluentes',
        ),
    
        row=1, col=2
    )
    fig2.add_trace(
        go.Bar(
        x = branches,
        y = sy,
        name = 'Limpios',
        ),
    
        row=1, col=2
    )


    fig2.add_trace(
        go.Table(
            header=dict(values=["Periodo", "Poluentes", "Limpios","Total"],font=dict(size=10),
                align="left"),
            cells=dict(values=[branches, fy,sy,[43,50,65]], align='left',height=30)
        ),
        row=1, col=1
    )
    fig2.update_layout(
        showlegend=True,
        coloraxis_showscale=False,
        barmode = 'stack',
        paper_bgcolor='#f8f9fa',
        title={
            'text': "Plan De Inversiones en Vehículos",
            'y':0.9,
            'x':0.5,
            'xanchor': 'center',
            'yanchor': 'top'},
        legend=dict(
                orientation="h",
                yanchor="bottom",
                y=-0.4,
                xanchor="right",
                x=1
            )
    )
    fig2.write_image("fig2.png")



    fig3 = make_subplots(
        rows=1, cols=2,
        shared_xaxes=True,
        vertical_spacing=0.01,
        specs=[[{"type": "icicle"},{"type": "bar"}]]
    )

    branches3 = [1,2,3]
    fy3 = [23,17,35]
    sy3 = [20, 23, 30]
    fig3.add_trace(
        go.Bar(
        x = branches3,
        y = fy3,
        name = 'Poluentes',
        ),
    
        row=1, col=2
    )
    fig3.add_trace(
        go.Bar(
        x = branches3,
        y = sy3,
        base=0,
        name = 'Limpios',
        ),
    
        row=1, col=2
    )

    labels = ["Costo combustible", "Limpios","Poluentes", "Tax carbon"]
    parents = ["", "Costo combustible", "Costo combustible", "Poluentes"]

    vendors = ["Tax carbon", None,None,None]
    sectors = ["Poluentes", "Limpios","Poluentes", "Limpios"]
    sales = [4, 6, 45, 45]

    df = pd.DataFrame(
        dict(vendors=vendors, sectors=sectors, sales=sales)
    )
    df["all"] = "all" # in order to have a single root node

    fig3.add_trace(
        go.Icicle(
        labels = labels,
        parents = parents,
        values = [100, 90, 10, 4],
        maxdepth=3,
        branchvalues="total",
        textinfo = "label+value",
    texttemplate ='<b>%{label} </b> <br> $%{value}',
        marker_colorscale = 'rdylgn'),
        row=1, col=1
    )
    fig3.update_layout(
        margin = dict(t=100, l=50, r=25, b=0),
        showlegend=True,
        coloraxis_showscale=False,
        paper_bgcolor='#f8f9fa',
        title={
            'text': "Costos combustibles",
            'y':0.9,
            'x':0.5,
            'xanchor': 'center',
            'yanchor': 'top'},
        legend=dict(
                orientation="h",
                yanchor="bottom",
                y=-0.3,
                xanchor="right",
                x=1
            ),
            uniformtext=dict(minsize=8, mode='show'),

    )
    fig3.write_image("fig3.png")




    fig4 = make_subplots(
        rows=1, cols=1,
        shared_xaxes=True,
        vertical_spacing=0.01,
        specs=[[{"type": "bar"}]]
    )

    branches4 = [1,2,3]
    combustible = [23,17,35]
    administracion = [20, 23, 30]
    financiacion = [40, 43, 20]
    mantenimiento = [50, 53, 10]
    descuento = [60, 3, 5]
    fig4.add_trace(
        go.Bar(
        x = branches4,
        y = combustible,
        name = 'Combustible ',
        ),
    
        row=1, col=1
    )
    fig4.add_trace(
        go.Bar(
        x = branches4,
        y = administracion,
        base=0,
        name = 'Administración  ',
        ),
    
        row=1, col=1
    )

    fig4.add_trace(
        go.Bar(
        x = branches4,
        y = financiacion,
        base=0,
        name = 'Financiación    ',
        ),
    
        row=1, col=1
    )
    fig4.add_trace(
        go.Bar(
        x = branches4,
        y = mantenimiento,
        base=0,
        name = 'Mantenimiento   ',
        ),
    
        row=1, col=1
    )
    fig4.add_trace(
        go.Bar(
        x = branches4,
        y = descuento,
        base=0,
        name = 'Descuento   ',
        ),
    
        row=1, col=1
    )



    fig4.update_layout(
        showlegend=True,
        coloraxis_showscale=False,
        paper_bgcolor='#f8f9fa',
        title={
            'text': "Evolución mensual de los costos",
            'y':0.9,
            'x':0.5,
            'xanchor': 'center',
            'yanchor': 'top'},
            legend=dict(
                orientation="h",
                yanchor="bottom",
                y=-0.2,
                xanchor="right",
                x=1
            )
    )
    fig4.write_image("fig4.png")

    graphs = [
        'girozero-logo'
        #'fig1'
    ]

    from IPython.display import display, HTML

    def report_block_template(report_type, graph_url, caption=''):
        if report_type == 'interactive':
            graph_block = '<iframe style="border: none;" src="{graph_url}.embed" width="100%" height="600px"></iframe>'
        elif report_type == 'static':
            graph_block = (''
                '<a href="{graph_url}" target="_blank">' # Open the interactive graph when you click on the image
                    '<img style="height: 400px;" src="{graph_url}.png">'
                '</a>')

        report_block = ('' +
            '''<table width="100%" cellpadding="0" cellspacing="0" bgcolor=" rgb(255,255,255)">
            <tr>
                <td>
                    <table id="top-message" cellpadding="20" cellspacing="0" width="600" align="center">
                        <tr>
                            <td align="center">
                                <p><a href="#"></a></p>
                            </td>
                        </tr>
                    </table>

                    <table id="main" width="600" align="center" cellpadding="0" cellspacing="15" bgcolor=" rgb(255,255,255)">
                        <tr>
                            <td>
                                <table id="header" cellpadding="10" cellspacing="0" align="center" bgcolor="#8fb3e9">
                                    <tr>
                                        <td width="585" align="center" bgcolor="#00A099">
                                            <h1>Simulador 2.0, reporte {{fecha}}</h1>
                                            <h1>{{compania}}-{{cargo}}</h1>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <table id="content-3" cellpadding="0" cellspacing="0" align="center">
                                    <tr>
                                        <td width="120" valign="top" style="padding:12px;">
                                            <img
                                                src="https://girozero.uniandes.edu.co/sites/default/files/PrincipalFondo_GZ.png" />
                                        </td>
                                        <td width="120" valign="top" style="padding:12px;">
                                            <img
                                                src="https://girozero.uniandes.edu.co/sites/default/files/inline-images/l1.jpg" />
                                        </td>
                                        <td width="30"></td>
                                        <td width="220" valign="top" style="padding:5px;">
                                            <img src="https://girozero.uniandes.edu.co/sites/default/files/inline-images/l2.jpg"
                                                width="220" />
                                        </td>
                                        <td width="220" valign="top" style="padding:5px;">
                                            <img src="https://girozero.uniandes.edu.co/sites/default/files/inline-images/l3.jpg"
                                                width="220" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td  bgcolor="#f8f9fa" align="center">
                                <img style="height: 300px;" src="fig1.png">
                                <img style="height: 300px;" src="fig2.png">
                            </td>
                        </tr>
                        <tr>
                            <td  bgcolor="#f8f9fa" align="center">
                                <img style="height: 300px;" src="fig3.png">
                                <img style="height: 300px;" src="fig4.png">

                            </td>
                        </tr>

                    </table>
                    <table id="bottom" cellpadding="20" cellspacing="0" width="600" align="center">
                        <tr>
                            <td align="center">
                                <p><a href="https://girozero.uniandes.edu.co/">girozero.uniandes.edu.co</a></p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>''') # horizontal line                       

        return report_block.format(graph_url=graph_url, caption=caption)


    interactive_report = ''
    static_report = ''

    for graph_url in graphs:
        _static_block = report_block_template('static', graph_url, caption='')
        _interactive_block = report_block_template('interactive', graph_url, caption='')

        static_report += _static_block
        interactive_report += _interactive_block


    #display(HTML(static_report))

    from xhtml2pdf import pisa             # import python module

    # Utility function
    def convert_html_to_pdf(source_html, output_filename):
        # open output file for writing (truncated binary)
        result_file = open(output_filename, "w+b")

        # convert HTML to PDF
        pisa_status = pisa.CreatePDF(
                source_html,                # the HTML to convert
                dest=result_file)           # file handle to recieve result

        # close output file
        result_file.close()                 # close output file

        # return True on success and False on errors
        return pisa_status.err

    convert_html_to_pdf(static_report, 'report.pdf')

 """