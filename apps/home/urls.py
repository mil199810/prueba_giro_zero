# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

from django.urls import path, re_path
from apps.home import views
from .views import CiudadUpdate, CiudadCreate

urlpatterns = [

    # The home page
    path('', views.index, name='home'),
    path('ciudades/', views.ciudades, name='ciudades'),
    path('ciudades_update/<int:pk>/', views.CiudadUpdate.as_view(), name="ciudades_update"),
    path('ciudades_delete/<int:id>/', views.CiudadDelete, name="ciudades_delete"),
    path('ciudades_create/', views.CiudadCreate.as_view(), name="ciudades_create"),
    path('clientes/', views.clientes, name='clientes'),
    path('clientes_update/<int:pk>/', views.ClienteUpdate.as_view(), name="clientes_update"),
    path('clientes_delete/<int:id>/', views.ClienteDelete, name="clientes_delete"),
    path('clientes_create/', views.ClienteCreate.as_view(), name="clientes_create"),
    path('usuarios/', views.usuarios, name='usuarios'),
    path('usuarios_update/<int:pk>/', views.UsuariosUpdate.as_view(), name="usuarios_update"),
    path('usuarios_delete/<int:id>/', views.UsuariosDelete, name="usuarios_delete"),
    path('usuarios_create/', views.UsuariosCreate.as_view(), name="usuarios_create"),
    
    # path('visualitation/<str:id>', views.visualitation, name='visualitation'),

    # Matches any html file
    re_path(r'^.*\.*', views.pages, name='pages'),

]
