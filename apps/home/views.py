# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""
import io
from django import template
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.template import loader
from django.urls import reverse
from django.views.generic import CreateView, UpdateView, ListView, View, DetailView, DeleteView
from .forms import CityForm, ClientForm, UserForm
from .models import City, Client, Users
from django.urls import reverse_lazy
import asyncio
import os
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
loop = asyncio.get_event_loop()

@login_required(login_url="/login/")
def index(request):
    context = {'segment': 'Inicio'}

    html_template = loader.get_template('home/index.html')
    return HttpResponse(html_template.render(context, request))



@login_required(login_url="/login/")
def pages(request):
    context = {}
    # All resource paths end in .html.
    # Pick out the html file name from the url. And load that template.
    try:

        load_template = request.path.split('/')[-1]

        if load_template == 'admin':
            return HttpResponseRedirect(reverse('admin:index'))
        context['segment'] = load_template

        html_template = loader.get_template('home/' + load_template)
        return HttpResponse(html_template.render(context, request))

    except template.TemplateDoesNotExist:

        html_template = loader.get_template('home/page-404.html')
        return HttpResponse(html_template.render(context, request))

    except:
        html_template = loader.get_template('home/page-500.html')
        return HttpResponse(html_template.render(context, request))

@login_required(login_url="/login/")
def ciudades(request):
    ciudades = City.objects.filter()
    context = {'segment': 'Ciudades', 'ciudades':ciudades}
    html_template = loader.get_template('home/ciudades.html')
    return HttpResponse(html_template.render(context, request))
    
class CiudadUpdate(UpdateView):
    model = City
    template_name = 'home/update.html'
    form_class = CityForm
    success_url = reverse_lazy('ciudades')


class CiudadCreate(CreateView):
    model = City
    template_name = 'home/create.html'
    form_class = CityForm
    success_url = reverse_lazy('ciudades')


def CiudadDelete(request,id):
    ciudad = City.objects.get(id=id)
    ciudad.delete()
    return redirect('ciudades')

@login_required(login_url="/login/")
def clientes(request):
    clientes = Client.objects.filter()
    context = {'segment': 'Clientes', 'clientes':clientes}
    html_template = loader.get_template('home/clientes.html')
    return HttpResponse(html_template.render(context, request))


class ClienteUpdate(UpdateView):
    model = Client
    template_name = 'home/update.html'
    form_class = ClientForm
    success_url = reverse_lazy('clientes')


class ClienteCreate(CreateView):
    model = Client
    template_name = 'home/create.html'
    form_class = ClientForm
    success_url = reverse_lazy('clientes')


def ClienteDelete(request,id):
    cliente = Client.objects.get(id=id)
    cliente.delete()
    return redirect('clientes')

@login_required(login_url="/login/")
def usuarios(request):
    usuarios = Users.objects.filter()
    context = {'segment': 'Usuarios', 'usuarios':usuarios}
    html_template = loader.get_template('home/usuarios.html')
    return HttpResponse(html_template.render(context, request))

class UsuariosUpdate(UpdateView):
    model = Users
    template_name = 'home/update.html'
    form_class = UserForm
    success_url = reverse_lazy('usuarios')


class UsuariosCreate(CreateView):
    model = Users
    template_name = 'home/create.html'
    form_class = UserForm
    success_url = reverse_lazy('usuarios')


def UsuariosDelete(request,id):
    user = Users.objects.get(id=id)
    user.delete()
    return redirect('usuarios')






    