# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class City(models.Model):
    city_id = models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
    name = models.CharField(max_length=16)
    passw = models.CharField(max_length=16)
    email = models.CharField(max_length=64)


    def __str__(self):
        return self.name

class Client(models.Model):
    client_id = models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
    name = models.CharField(max_length=16,null=True,blank=True)
    city_id = models.ForeignKey(City, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.city_id.name


class Users(models.Model):
    name = models.CharField(max_length=16,null=True,blank=True)
    city_id = models.ForeignKey(City, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.city_id.name