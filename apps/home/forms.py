# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

from django import forms
from .models import City, Client, Users

class CityForm(forms.ModelForm):
    class Meta:
        model = City
        fields = ('name', 'passw', 'email')
        widgets = {
            'name': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Nombre',
                }
            ),
            'email': forms.EmailInput(
                attrs={
                    'class': 'form-control form-control-user',
                    'placeholder': 'Nombre',
                }
            ),
            'passw': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Pass',
                }
            ),

        }


class ClientForm(forms.ModelForm):
    class Meta:
        model = Client
        fields = ('name', 'city_id')
        widgets = {
            'name': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Nombre',
                }
            ),
            'city_id': forms.Select(
                attrs={
                    'class': 'form-control',
                }
            ),

        }

class UserForm(forms.ModelForm):
    class Meta:
        model = Users
        fields = ('name', 'city_id')
        widgets = {
            'name': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Nombre',
                }
            ),
            'city_id': forms.Select(
                attrs={
                    'class': 'form-control',
                }
            ),

        }
