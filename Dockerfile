FROM python:3.9

COPY . .

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

#RUN apt-get update && apt-get install -y curl

# Install individual modules (alternative to installing the full bundle)
#RUN cd /opt/ && curl -OL https://portal.ampl.com/dl/bundles/5f7ccfe4-7bd7-11ed-be0f-0cc47aaa0f0a/bundle1873069124/ampl_linux-intel64.tgz && \
#    tar oxzvf ampl_linux-intel64.tgz && rm ampl_linux-intel64.tgz
#ARG MODULES_URL=https://ampl.com/dl/modules
# Download ampl-module.linux64.tgz
#RUN cd /opt/ && curl -OL ${MODULES_URL}/ampl-module.linux64.tgz && \
#    tar oxzvf ampl-module.linux64.tgz && rm ampl-module.linux64.tgz
# Download gurobi-module.linux64.tgz
#RUN cd /opt/ && curl -OL ${MODULES_URL}/gurobi-module.linux64.tgz && \
#    tar oxzvf gurobi-module.linux64.tgz && rm gurobi-module.linux64.tgz

# Add installation directory to the environment variable PATH
#COPY ampl.linux-intel64 /opt/ampl.linux-intel64/
#RUN rm -rf ampl.linux-intel64
#COPY ampl.lic /tmp/ampl/
#ENV PATH="/opt/ampl.linux-intel64/:${PATH}"
#ENV AMPLKEY_RUNTIME_DIR /tmp/ampl/
#ENV AMPL_LICFILE /tmp/ampl/ampl.lic

# install python dependencies
RUN pip install --upgrade pip
RUN pip install --no-cache-dir -r requirements.txt

# running migrations
RUN python manage.py migrate

# gunicorn
CMD ["gunicorn", "--config", "gunicorn-cfg.py", "core.wsgi"]

